package com.toptal.screening.quizapp.controller;

import com.toptal.screening.quizapp.dto.QuizBase;
import com.toptal.screening.quizapp.model.QuizSubmission;
import com.toptal.screening.quizapp.service.QuizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class QuizController {

    @Autowired
    private QuizService quizService;

    @PostMapping("/submitQuiz")
    public QuizBase submitQuiz (@RequestBody QuizSubmission quiz, @RequestHeader Map<String, String> headers) throws Exception {
        return quizService.submitQuiz(headers.get("username"), quiz);
    }


    @GetMapping("/myQuizzes")
    public List<QuizBase> getQuizzesTaken (@RequestParam(name = "pageNum", defaultValue = "0") int pageNum,
                                           @RequestParam(name = "pageSize", defaultValue = "10") int pageSize,
                                           @RequestHeader Map<String, String> headers) throws Exception {

        return quizService.getQuizzesTaken(headers.get("username"), pageNum, pageSize);
    }


}
