package com.toptal.screening.quizapp.exception;

public class InvalidQuizException extends QuizMasterException{

    public InvalidQuizException (String msg){
        super(msg);
    }
}
