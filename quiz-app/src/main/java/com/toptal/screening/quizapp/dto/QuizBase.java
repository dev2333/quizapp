package com.toptal.screening.quizapp.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class QuizBase {

    private String quizName;
    private String quizId;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double score;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String participant;
}
