package com.toptal.screening.quizapp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Data
@Getter
@Setter
@Entity
@Table(name = "t_quiz_question")
public class QuizQuestion {
    @Column(name="question")
    private String question;

    @OneToMany(mappedBy="question",fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<QuizAnswer> answers;

    @Column(name="isMultiAnswer")
    private boolean isMultiAnswer;

    @Column(name="answerCount")
    private int correctAnswerCount;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long questionId;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="quiz_id", nullable=false)
    private Quiz quiz;

}
