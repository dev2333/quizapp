package com.toptal.screening.quizapp.datastore;

import com.toptal.screening.quizapp.exception.InvalidQuizException;
import com.toptal.screening.quizapp.model.Quiz;
import com.toptal.screening.quizapp.repo.QuizRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class QuizDataStore {

    @Autowired
    private QuizRepo quizRepo;

    public Quiz getQuizById (String quizId) throws Exception {
        Quiz savedQuiz = quizRepo.findById(quizId).get();
        if(savedQuiz == null) {
            throw new InvalidQuizException(quizId + " invalid quiz");
        }
        return savedQuiz;
    }

}
