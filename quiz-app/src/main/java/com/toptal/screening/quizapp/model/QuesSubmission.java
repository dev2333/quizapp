package com.toptal.screening.quizapp.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name="t_quiz_ques_submit")
public class QuesSubmission {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    private Date quesAttendedDate;
    private String attendee;
    private Double score;
    private String question;

    @Transient
    private Set<String> answers;

    @ManyToOne
    @JoinColumn(name="question_id", nullable=false)
    private QuizSubmission quiz;
}
