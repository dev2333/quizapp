package com.toptal.screening.quizapp.serviceImpl;

import com.toptal.screening.quizapp.datastore.QuizDataStore;
import com.toptal.screening.quizapp.datastore.QuizStatsStore;
import com.toptal.screening.quizapp.dto.QuizBase;
import com.toptal.screening.quizapp.model.Quiz;
import com.toptal.screening.quizapp.model.QuizSubmission;
import com.toptal.screening.quizapp.service.QuizService;
import com.toptal.screening.quizapp.validator.ScoreComputationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuizServiceImpl implements QuizService {

    @Autowired
    private QuizDataStore dataStore;

    @Autowired
    private QuizStatsStore statsDataStore;


    @Override
    public QuizBase submitQuiz (String userName, QuizSubmission submission) throws Exception {
        submission.setParticipant(userName);
        Quiz quiz = dataStore.getQuizById(submission.getQuizId());
        QuizBase quizBase= ScoreComputationUtil.computeQuizScore(submission,quiz);
        return quizBase;
    }

    @Override
    public List<QuizBase> getQuizzesTaken (String userName, int pageNum, int pageSize) throws Exception {
        return statsDataStore.getQuizzesTaken(userName,pageNum,pageSize);
    }


}
