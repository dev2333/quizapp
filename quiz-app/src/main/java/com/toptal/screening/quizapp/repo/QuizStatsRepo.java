package com.toptal.screening.quizapp.repo;

import com.toptal.screening.quizapp.model.QuizSubmission;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuizStatsRepo extends JpaRepository<QuizSubmission,String> {

    List<QuizSubmission> findAllByParticipant (String participant, Pageable pageable);
}
