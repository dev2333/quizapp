package com.toptal.screening.quizapp.exception;

public class QuizMasterException extends Exception{
    public QuizMasterException(String msg){
        super(msg);
    }
}
