package com.toptal.screening.quizapp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "quiz_answer")
public class QuizAnswer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long answerId;

    @Column(name="answer")
    private String answer;
    @Column(name="isCorrect")
    private boolean isCorrect;

    @ToString.Exclude
    @ManyToOne
    @JsonIgnore
    @JoinColumn(name="question_id", nullable=false)
    private QuizQuestion question;



}
