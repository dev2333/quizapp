package com.toptal.screening.quizapp.validator;

import com.toptal.screening.quizapp.dto.QuizBase;
import com.toptal.screening.quizapp.model.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ScoreComputationUtil {

    public static QuizBase computeQuizScore(QuizSubmission submission, Quiz quiz){
        QuizBase quizStatus = new QuizBase();
        quizStatus.setQuizId(submission.getQuizId());
        quizStatus.setQuizName(submission.getQuizName());
        quizStatus.setParticipant(submission.getParticipant());
        Map<String, Set<String>> ansMap =new HashMap<>();


        int totalCount = 0;
        for(QuizQuestion question :quiz.getQuestions()){
           Set<String> correctAnswers = new HashSet<>();
            for(QuizAnswer answer:question.getAnswers()){
                if(answer.isCorrect()){
                    correctAnswers.add(answer.getAnswer());
                    totalCount++;
                }
            }
            ansMap.put(question.getQuestion(),correctAnswers);
        }

        int totalCorrectCount =0;
        for(QuesSubmission question :submission.getQuestions()){
            Set<String>correctAnswers =ansMap.get(question.getQuestion());
            int correctCount =0;
            if(correctAnswers.size()>0) {
                for (String answer : question.getAnswers()) {
                    if(correctAnswers.contains(answer)) {
                        correctCount++;
                        totalCorrectCount++;
                    }
                }
            }

            question.setScore(getQuesScore((double)correctCount/correctAnswers.size()));
        }
        quizStatus.setScore(getQuesScore((double)(totalCorrectCount / totalCount)));

        return  quizStatus;
    }

    private static double getQuesScore (double score) {
        score = score *100;
        score = Math.round(score);
        score = score /100;
        return score;
    }
}
