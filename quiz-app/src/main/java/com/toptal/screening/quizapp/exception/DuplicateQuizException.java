package com.toptal.screening.quizapp.exception;

public class DuplicateQuizException extends QuizMasterException{

    public DuplicateQuizException(String msg){
        super(msg);
    }
}
