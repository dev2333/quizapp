package com.toptal.screening.quizapp.datastore;

import com.toptal.screening.quizapp.dto.QuizBase;
import com.toptal.screening.quizapp.model.QuizSubmission;
import com.toptal.screening.quizapp.repo.QuizStatsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class QuizStatsStore {

    @Autowired
    private QuizStatsRepo quizStatsRepo;

    public void saveQuizParticipation(QuizSubmission submission , String participant){
        quizStatsRepo.save(submission);
    }

    public List<QuizBase> getQuizzesTaken(String userName, int pageNum, int pageSize){
        List<QuizBase> quizStats = new ArrayList<>();
        Pageable pageable = PageRequest.of(pageNum, pageSize);

        for(QuizSubmission quizSubmission:quizStatsRepo.findAllByParticipant(userName,pageable)){
            quizStats.add(new QuizBase(quizSubmission.getQuizName(),quizSubmission.getQuizId(),quizSubmission.getMark(),quizSubmission.getParticipant()));
        }
        return quizStats;

    }

}
