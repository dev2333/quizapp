package com.toptal.screening.quizapp.service;


import com.toptal.screening.quizapp.dto.QuizBase;
import com.toptal.screening.quizapp.model.QuizSubmission;

import java.util.List;

public interface QuizService {

    public QuizBase submitQuiz (String userName, QuizSubmission quiz) throws Exception;
    public List<QuizBase> getQuizzesTaken (String userName,int pageNum, int pageSize) throws Exception;

}
