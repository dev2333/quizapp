package com.toptal.screening.quizapp.exception.advice;

import com.toptal.screening.quizapp.exception.DuplicateQuizException;
import com.toptal.screening.quizapp.exception.InvalidQuizException;
import com.toptal.screening.quizapp.exception.QuizMasterException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class QuizMasterControllerAdvice  extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = { QuizMasterException.class, DuplicateQuizException.class, InvalidQuizException.class})
    public ResponseEntity<Object> handleQuizException(Exception ex, WebRequest request) {
        String bodyOfResponse = ex.getMessage();
        System.out.println("............");
        ex.printStackTrace();
        return handleExceptionInternal(ex, bodyOfResponse,
                new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
}
