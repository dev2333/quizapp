package com.toptal.screening.quizmaster.model;

import com.toptal.screening.quizmaster.model.QuizQuestion;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;


@Getter
@Setter
@Table(name = "t_quiz_submit")
@Entity
public class QuizSubmission {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;

    private String quizName;
    private Double mark;
    private String participant;

    private String quizId;

    @OneToMany(mappedBy="quiz",fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<QuesSubmission> questions;

}
