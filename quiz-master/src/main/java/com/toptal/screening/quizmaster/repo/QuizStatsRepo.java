package com.toptal.screening.quizmaster.repo;

import com.toptal.screening.quizmaster.model.Quiz;
import com.toptal.screening.quizmaster.model.QuizSubmission;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuizStatsRepo extends PagingAndSortingRepository<QuizSubmission,String> {

    List<QuizSubmission> findAllByQuizId (String quizId, Pageable pageable);
}
