package com.toptal.screening.quizmaster.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.FetchMode;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name = "t_quiz")
public class Quiz {

    @OneToMany(mappedBy="quiz",fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<QuizQuestion> questions;

    @Column(name="quizMaster")
    private String quizMaster;
    @Column(name="quizName")
    private String quizName;

    @Column(name="isDraft")
    private boolean isDraft;

    @Id
    @Column(name="quiz_Id")
    private String quizId;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modify_date")
    private Date updatedDate;
}
