package com.toptal.screening.quizmaster.exception;

public class QuizMasterException extends Exception{
    public QuizMasterException(String msg){
        super(msg);
    }
}
