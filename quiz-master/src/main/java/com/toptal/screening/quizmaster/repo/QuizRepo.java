package com.toptal.screening.quizmaster.repo;

import com.toptal.screening.quizmaster.model.Quiz;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


import java.util.List;

@Repository
public interface QuizRepo extends PagingAndSortingRepository<Quiz,String> {

    @Query("SELECT q FROM Quiz q WHERE quizMaster = ?1 and isDraft=false")
    List<Quiz> findAllByUserAndPublished (String user, Pageable pageable);

    @Query("SELECT q FROM Quiz q WHERE quizMaster = ?1 ")
    List<Quiz> findAllByUser (String user, Pageable pageable);
}
