package com.toptal.screening.quizmaster.datastore;

import com.toptal.screening.quizmaster.constants.QuizSearchOrderByEnum;
import com.toptal.screening.quizmaster.dto.QuizBase;
import com.toptal.screening.quizmaster.exception.InvalidQuizException;
import com.toptal.screening.quizmaster.model.Quiz;
import com.toptal.screening.quizmaster.model.QuizQuestion;
import com.toptal.screening.quizmaster.repo.QuizRepo;
import com.toptal.screening.quizmaster.util.QuizIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class QuizDataStore {

   /* private Map<String, QuizMetaData> userQuizMap = new ConcurrentHashMap<>();
    private Map<String,Quiz> quizzes = new ConcurrentHashMap<>();*/

    @Autowired
    private QuizRepo quizRepo;

    public Quiz saveQuiz (String userName, Quiz quiz) throws Exception {

        quiz.setQuizId(QuizIdGenerator.getHashUuid(userName));
        quiz.getQuestions().stream().forEach(q->q.setQuiz(quiz));
        for (QuizQuestion question:quiz.getQuestions()){
            question.getAnswers().stream().forEach(a->a.setQuestion(question));
        }

        quizRepo.save(quiz);
        return quiz;

    }

    public Quiz updateQuiz (String userName, Quiz quiz) throws Exception {

        Quiz savedQuiz = quizRepo.findById(quiz.getQuizId()).get();
        if(savedQuiz == null) {
            throw new InvalidQuizException(quiz.getQuizId() + " invalid quiz");
        }
        if(!savedQuiz.getQuizMaster().equals(userName)) {
            throw new InvalidQuizException(quiz.getQuizId() + " not authorised update quiz");
        }
        if(!savedQuiz.isDraft()) {
            throw new InvalidQuizException(quiz.getQuizId() + " is already published");
        }
        quiz.setUpdatedDate(new Date());
        quizRepo.save(quiz);

        return quiz;
    }

    public Quiz publishQuiz (String userName, Quiz quiz) throws Exception {
        updateQuiz(userName, quiz);
        Quiz savedQuiz = quizRepo.findById(quiz.getQuizId()).get();
        savedQuiz.setDraft(false);
        savedQuiz.setUpdatedDate(new Date());
        quizRepo.save(savedQuiz);

        return quiz;
    }

    public void deleteQuiz (String userName, String quizId) throws Exception {
        Quiz savedQuiz = quizRepo.findById(quizId).get();
        if(savedQuiz == null) {
            throw new InvalidQuizException(quizId + " invalid quiz");
        }
        if(!savedQuiz.getQuizMaster().equals(userName)) {
            throw new InvalidQuizException(quizId + " not authorised update quiz");
        }
        quizRepo.delete(savedQuiz);

    }

    public Quiz getQuiz (String userName, String quizId) throws Exception {
       Quiz savedQuiz = quizRepo.findById(quizId).get();
        if(savedQuiz == null) {
            throw new InvalidQuizException(quizId + " invalid quiz");
        }
        if(!savedQuiz.getQuizMaster().equals(userName)) {
            throw new InvalidQuizException(quizId + " not authorised update quiz");
        }
        Quiz quiz = new Quiz();
        return quiz;

    }

    public List<QuizBase> getPublishedQuizzes (String userName, int pageNum, int pageSize,
                                               QuizSearchOrderByEnum orderBy) throws Exception {
        List<QuizBase> publishedQuizzes = new ArrayList<>();

        Pageable pageable = PageRequest.of(pageNum, pageSize);
        List<Quiz> quizzes = quizRepo.findAllByUserAndPublished(userName, pageable);
        for (Quiz quiz : quizzes) {

            publishedQuizzes.add(new QuizBase(quiz.getQuizName(), quiz.getQuizId(), null,null));
        }

        return publishedQuizzes;

    }

    public List<QuizBase> getAllQuizzes (String userName, int pageNum, int pageSize,
                                         QuizSearchOrderByEnum orderBy) throws Exception {

        List<QuizBase> publishedQuizzes = new ArrayList<>();

        Pageable pageable = PageRequest.of(pageNum, pageSize);
        List<Quiz> quizzes = quizRepo.findAllByUser(userName, pageable);
        for (Quiz quiz : quizzes) {

            publishedQuizzes.add(new QuizBase(quiz.getQuizName(), quiz.getQuizId(), null,null));
        }

        return publishedQuizzes;

    }


}
