package com.toptal.screening.quizmaster.controller;

import com.toptal.screening.quizmaster.model.QuizSubmission;
import com.toptal.screening.quizmaster.service.QuizMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class QuizStatsController {
    @Autowired
    private QuizMasterService quizMasterService;

    @PutMapping("/saveQuizParticipation")
    public void saveQuizStats (@RequestBody QuizSubmission submission, @RequestParam String quizParticipant) throws Exception {
       quizMasterService.saveQuizStats(submission,quizParticipant);
    }

}
