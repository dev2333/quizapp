package com.toptal.screening.quizmaster.service;


import com.toptal.screening.quizmaster.constants.QuizSearchOrderByEnum;
import com.toptal.screening.quizmaster.constants.QuizStatsOrderByEnum;
import com.toptal.screening.quizmaster.dto.QuizBase;
import com.toptal.screening.quizmaster.exception.QuizMasterException;
import com.toptal.screening.quizmaster.model.QuizSubmission;
import com.toptal.screening.quizmaster.model.Quiz;

import java.util.List;

public interface QuizMasterService {

    public Quiz addQuiz (String userName, Quiz quiz) throws Exception;
    public Quiz updateQuiz (String userName,Quiz quiz) throws Exception;
    public Quiz getQuiz (String userName,String quizId) throws Exception;
    public Quiz publishQuiz (String userName, Quiz quiz) throws Exception;
    public List<QuizBase> getPublishedQuizzes (String userName,int pageNum, int pageSize, QuizSearchOrderByEnum orderBy) throws Exception;
    public List<QuizBase> getAllQuizzes (String userName, int pageNum, int pageSize, QuizSearchOrderByEnum orderBy) throws Exception;
    public List<QuizBase> getQuizStatistics   (String userName,String quizId, int pageNum, int pageSize, QuizStatsOrderByEnum orderBy) throws QuizMasterException;
    public void deleteQuiz(String userName,String quizId) throws Exception;

    public void saveQuizStats(QuizSubmission quiz, String participant) throws Exception;
}
