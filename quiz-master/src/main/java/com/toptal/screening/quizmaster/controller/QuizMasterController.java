package com.toptal.screening.quizmaster.controller;

import com.toptal.screening.quizmaster.constants.QuizSearchOrderByEnum;
import com.toptal.screening.quizmaster.constants.QuizStatsOrderByEnum;
import com.toptal.screening.quizmaster.dto.QuizBase;
import com.toptal.screening.quizmaster.exception.QuizMasterException;
import com.toptal.screening.quizmaster.model.Quiz;
import com.toptal.screening.quizmaster.service.QuizMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class QuizMasterController {

    @Autowired
    private QuizMasterService quizMasterService;

    @PostMapping("/addQuiz")
    public Quiz addQuiz (@RequestBody Quiz quiz, @RequestHeader Map<String, String> headers) throws Exception {
        return quizMasterService.addQuiz(headers.get("username"), quiz);
    }


    @PutMapping("/updateQuiz")
    public Quiz updateQuiz (@RequestBody Quiz quiz, @RequestHeader Map<String, String> headers) throws Exception {
        return quizMasterService.updateQuiz(headers.get("username"), quiz);
    }

    @PutMapping("/publishQuiz")
    public Quiz publishQuiz (@RequestBody Quiz quiz, @RequestHeader Map<String, String> headers) throws Exception {

        return quizMasterService.publishQuiz(headers.get("username"), quiz);
    }

    @GetMapping("/publishedQuizzes")
    public List<QuizBase> getPublishedQuizzes (@RequestParam(name = "pageNum", defaultValue = "1") int pageNum,
                                               @RequestParam(name = "pageSize", defaultValue = "10") int pageSize,
                                               @RequestParam(name = "orderBy", defaultValue = "updatedDate") QuizSearchOrderByEnum orderBy,
                                               @RequestHeader Map<String, String> headers) throws Exception {

        return quizMasterService.getPublishedQuizzes(headers.get("username"), pageNum, pageSize, orderBy);
    }

    @GetMapping("/quizzes")
    public List<QuizBase> getAllQuizzes (@RequestParam(name = "pageNum", defaultValue = "0") int pageNum,
                                     @RequestParam(name = "pageSize", defaultValue = "10") int pageSize,
                                     @RequestParam(name = "orderBy", defaultValue = "updatedDate") QuizSearchOrderByEnum orderBy,
                                     @RequestHeader Map<String, String> headers) throws Exception {

        return quizMasterService.getAllQuizzes(headers.get("username"), pageNum, pageSize, orderBy);
    }

    @GetMapping("/quiz/stats/{quizId}")
    public List<QuizBase> getQuizStats (@PathVariable("quizId") String quizId, @RequestParam(name = "pageNum", defaultValue = "1") int pageNum,
                                @RequestParam(name = "pageSize", defaultValue = "10") int pageSize,
                                @RequestParam(name = "orderBy", defaultValue = "takenDate") QuizStatsOrderByEnum orderBy,
                                @RequestHeader Map<String, String> headers) throws QuizMasterException {

        return quizMasterService.getQuizStatistics(headers.get("username"), quizId, pageNum, pageSize, orderBy);
    }

    @GetMapping("/quiz/{quizId}")
    public Quiz getQuiz (@PathVariable("quizId") String quizId,
                         @RequestHeader Map<String, String> headers) throws Exception {

        return quizMasterService.getQuiz(headers.get("username"), quizId);
    }

    @DeleteMapping("/delete/{quizId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteQuiz (@PathVariable("quizId") String quizId, @RequestHeader Map<String, String> headers) throws Exception {

        quizMasterService.deleteQuiz(headers.get("username"), quizId);
    }


}
