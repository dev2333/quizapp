package com.toptal.screening.quizmaster.validator;

import com.toptal.screening.quizmaster.exception.InvalidQuizException;
import com.toptal.screening.quizmaster.model.Quiz;
import com.toptal.screening.quizmaster.model.QuizAnswer;
import com.toptal.screening.quizmaster.model.QuizQuestion;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;


public class QuizValidator {

    public static void validateQuiz (Quiz quiz) throws InvalidQuizException {

        if(!StringUtils.hasLength(quiz.getQuizName())) {
            throw new InvalidQuizException("quiz name mandatory");
        }
        if(CollectionUtils.isEmpty(quiz.getQuestions())) {
            throw new InvalidQuizException("At least one question mandatory to create quiz ");
        }
        if(quiz.getQuestions().size() > 10) {
            throw new InvalidQuizException("Exceed max question limit");
        }
        long emptyQuestionCount = quiz.getQuestions().parallelStream().filter(q -> !StringUtils.hasLength(q.getQuestion())).count();

        if(emptyQuestionCount > 0l) {
            throw new InvalidQuizException("Question can't be empty");
        }
        for (QuizQuestion question : quiz.getQuestions()) {
            if(CollectionUtils.isEmpty(question.getAnswers())) {
                throw new InvalidQuizException("At least one answer required for a question");
            }
            if(question.getAnswers().size() > 5) {
                throw new InvalidQuizException("Answer limit exceeded for question");
            }

            int answerCount = 0;
            for (QuizAnswer answer : question.getAnswers()) {
                if(answer.isCorrect())
                    answerCount++;
                if(!StringUtils.hasLength(answer.getAnswer())){
                    throw new InvalidQuizException("Answer can't be null");
                }
            }
            if(question.isMultiAnswer() && answerCount<=1){
                throw new InvalidQuizException("for Multi Answer question answer count must be more more than one");
            }else if(!question.isMultiAnswer() && answerCount!=1){
                throw new InvalidQuizException("for Single Answer question answer count must be one");
            }
            question.setCorrectAnswerCount(answerCount);

        }


    }
}
