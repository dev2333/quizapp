package com.toptal.screening.quizmaster.datastore;

import com.toptal.screening.quizmaster.constants.QuizStatsOrderByEnum;
import com.toptal.screening.quizmaster.dto.QuizBase;
import com.toptal.screening.quizmaster.exception.QuizMasterException;
import com.toptal.screening.quizmaster.model.Quiz;
import com.toptal.screening.quizmaster.model.QuizSubmission;
import com.toptal.screening.quizmaster.repo.QuizRepo;
import com.toptal.screening.quizmaster.repo.QuizStatsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class QuizStatsStore {

    @Autowired
    private QuizStatsRepo quizStatsRepo;

    @Autowired
    private QuizRepo quizRepo;

    public void saveQuizParticipation(QuizSubmission submission ,String participant){
        submission.setParticipant(participant);
        quizStatsRepo.save(submission);
    }

    public List<QuizBase> getQuizStats(String owner, String quizId,int pageNum, int pageSize, QuizStatsOrderByEnum orderBy) throws QuizMasterException {
        List<QuizBase> quizStats = new ArrayList<>();

        Quiz quiz = quizRepo.findById(quizId).get();
        if(quiz ==null  || !quiz.getQuizMaster().equals(owner)){
            throw new QuizMasterException("not authorized");
        }
        Pageable pageable = PageRequest.of(pageNum, pageSize);
        for(QuizSubmission quizSubmission:quizStatsRepo.findAllByQuizId(quizId,pageable)){
            quizStats.add(new QuizBase(quizSubmission.getQuizName(),quizSubmission.getQuizId(),quizSubmission.getMark(),quizSubmission.getParticipant()));
        }
        return quizStats;
    }

}
