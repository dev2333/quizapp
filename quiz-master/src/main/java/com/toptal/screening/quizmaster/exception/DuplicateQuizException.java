package com.toptal.screening.quizmaster.exception;

public class DuplicateQuizException extends QuizMasterException{

    public DuplicateQuizException(String msg){
        super(msg);
    }
}
