package com.toptal.screening.quizmaster.dto;

import lombok.*;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class QuizMetaData {

    private Set<String> quizNames = new HashSet<>();
    private Map<String,String>quizIdMap = new ConcurrentHashMap<>();

}
