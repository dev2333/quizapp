package com.toptal.screening.quizmaster.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name="t_quiz_ques_submit")
public class QuesSubmission {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;

    private Date quesAttendedDate;
    private String attendee;
    private Double score;
    private String question;

    @ManyToOne
    @JoinColumn(name="question_id", nullable=false)
    private QuizSubmission quiz;
}
