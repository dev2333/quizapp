package com.toptal.screening.quizmaster.exception;

public class InvalidQuizException extends QuizMasterException{

    public InvalidQuizException (String msg){
        super(msg);
    }
}
