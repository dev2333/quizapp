package com.toptal.screening.quizmaster.serviceImpl;

import com.toptal.screening.quizmaster.constants.QuizSearchOrderByEnum;
import com.toptal.screening.quizmaster.constants.QuizStatsOrderByEnum;
import com.toptal.screening.quizmaster.datastore.QuizDataStore;
import com.toptal.screening.quizmaster.datastore.QuizStatsStore;
import com.toptal.screening.quizmaster.dto.QuizBase;
import com.toptal.screening.quizmaster.exception.QuizMasterException;
import com.toptal.screening.quizmaster.model.QuizSubmission;
import com.toptal.screening.quizmaster.model.Quiz;
import com.toptal.screening.quizmaster.service.QuizMasterService;
import com.toptal.screening.quizmaster.validator.QuizValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuizMasterServiceImpl implements QuizMasterService {

    @Autowired
    private QuizDataStore dataStore;

    @Autowired
    private QuizStatsStore statsDataStore;


    @Override
    public Quiz addQuiz (String userName, Quiz quiz) throws Exception {
        quiz.setQuizMaster(userName);
        QuizValidator.validateQuiz(quiz);
        return dataStore.saveQuiz(userName,quiz);
    }

    @Override
    public Quiz updateQuiz (String userName, Quiz quiz) throws Exception {
        quiz.setQuizMaster(userName);
        QuizValidator.validateQuiz(quiz);
        return dataStore.updateQuiz(userName,quiz);
    }

    @Override
    public Quiz publishQuiz (String userName,Quiz quiz) throws Exception {
        quiz.setQuizMaster(userName);
        QuizValidator.validateQuiz(quiz);
        return dataStore.publishQuiz(userName,quiz);
    }

    @Override
    public List<QuizBase> getPublishedQuizzes (String userName, int pageNum, int pageSize, QuizSearchOrderByEnum orderBy) throws Exception {
        return dataStore.getPublishedQuizzes(userName,pageNum,pageSize,orderBy);
    }

    @Override
    public List<QuizBase> getAllQuizzes (String userName, int pageNum, int pageSize, QuizSearchOrderByEnum orderBy) throws Exception {
        return dataStore.getAllQuizzes(userName,pageNum,pageSize,orderBy);
    }

    @Override
    public List<QuizBase> getQuizStatistics (String userName, String quizId, int pageNum, int pageSize, QuizStatsOrderByEnum orderBy) throws QuizMasterException {
        return statsDataStore.getQuizStats(userName,quizId,pageNum,pageSize,orderBy);
    }
    @Override
    public void deleteQuiz(String userName,String quizId) throws Exception {
        dataStore.deleteQuiz(userName,quizId);

    }

    @Override
    public void saveQuizStats (QuizSubmission quiz, String participant) throws Exception {

        statsDataStore.saveQuizParticipation(quiz,participant);

    }
    public Quiz getQuiz (String userName,String quizId) throws Exception {
        return dataStore.getQuiz(userName,quizId);
    }
}
