package com.toptal.screening.quiz.quizdatastore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuizDataStoreApplication {

	public static void main(String[] args) {

		try{SpringApplication.run(QuizDataStoreApplication.class, args);
		} catch (Exception e){
			e.printStackTrace();
		};
	}

}
