package com.toptal.screening.quiz.quizauthserver.service;

import com.toptal.screening.quiz.quizauthserver.dto.AuthResponse;
import com.toptal.screening.quiz.quizauthserver.dto.LoginRequest;
import com.toptal.screening.quiz.quizauthserver.dto.SignupRequest;
import com.toptal.screening.quiz.quizauthserver.model.User;
import com.toptal.screening.quiz.quizauthserver.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


@Service
public class UserDetailsImpl {
  @Autowired
  UserRepository userRepo;



  @Autowired
  JwtUtils jwtUtils;


  public AuthResponse validateUser(LoginRequest loginRequest) throws Exception {

    User user = userRepo.findByUsername(loginRequest.getUsername());

    if(user==null){
      throw  new Exception("invalid user");
    }
    if(new BCryptPasswordEncoder().matches(loginRequest.getPassword(),user.getPassword())){
      return  new AuthResponse(jwtUtils.generateJwtToken(user,1000000l),jwtUtils.generateJwtToken(user,1000000l));
    }else{
      throw  new Exception("invalid user");
    }



  }

  public AuthResponse registerUser(SignupRequest signupRequest) throws Exception {

    if(userRepo.existsByEmail(signupRequest.getEmail())){
      throw new Exception("Email already in Use");
    }
    User user = new User();
    user.setEmail(signupRequest.getEmail());
    user.setUsername(signupRequest.getEmail());
    user.setPassword( new BCryptPasswordEncoder().encode(signupRequest.getPassword()));
    userRepo.save(user);

    return  new AuthResponse(jwtUtils.generateJwtToken(user,1000000l),jwtUtils.generateJwtToken(user,1000000l));

  }


}
