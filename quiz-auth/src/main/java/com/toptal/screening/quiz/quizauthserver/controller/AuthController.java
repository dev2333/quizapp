package com.toptal.screening.quiz.quizauthserver.controller;

import com.toptal.screening.quiz.quizauthserver.dto.AuthResponse;
import com.toptal.screening.quiz.quizauthserver.dto.LoginRequest;
import com.toptal.screening.quiz.quizauthserver.dto.SignupRequest;
import com.toptal.screening.quiz.quizauthserver.service.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
  @Autowired
  UserDetailsImpl userDetails;

  @PostMapping("/signin")
  public AuthResponse authenticateUser(@Valid @RequestBody LoginRequest loginRequest) throws Exception {
   return  userDetails.validateUser(loginRequest);
  }

  @PostMapping("/signup")
  public AuthResponse registerUser(@Valid @RequestBody SignupRequest signUpRequest) throws Exception {
    return userDetails.registerUser(signUpRequest);
  }
}