package com.toptal.screening.quiz.quizauthserver.model;

import com.toptal.screening.quiz.quizauthserver.constants.RoleEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Role {

    private RoleEnum role;
}
